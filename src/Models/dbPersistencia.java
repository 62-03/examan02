package Models;
import java.util.ArrayList;
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public ArrayList listar() throws Exception;
    public Object buscar(String codigo) throws Exception;
    public boolean isExiste(String codigo) throws Exception;
}
