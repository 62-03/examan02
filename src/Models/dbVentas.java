package Models;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class dbVentas extends dbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objeto) throws Exception {
        Ventas ventas = new Ventas();
        ventas = (Ventas) objeto;
        String consulta = "";
        consulta = "insert into " + "ventas(codigo, tipo, precio, cantidad, total)values(?,?,?,?,?)";

        if (this.Conectar()) {
            try {
                System.err.println("se conecto");

                this.sqlConsulta = conexion.prepareStatement(consulta);
                //asignar valores a la consulta
                this.sqlConsulta.setString(1, ventas.getCodigo());
                this.sqlConsulta.setString(2, Integer.toString(ventas.getTipo()));
                if (ventas.getTipo() == 1) {
                    this.sqlConsulta.setString(3, Double.toString(24.50f));
                } else {
                    this.sqlConsulta.setString(3, Double.toString(20.50f));
                }
                this.sqlConsulta.setString(4, Integer.toString(ventas.getCantidad()));
                this.sqlConsulta.setString(5, Double.toString(ventas.getTotal()));
                this.sqlConsulta.executeUpdate();
                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al insertar; " + e.getMessage());
            }
        }
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Ventas> lista = new ArrayList<Ventas>();
        Ventas ventas;

        if (this.Conectar()) {
            String consulta = "SELECT * from ventas order by codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                ventas = new Ventas();
                ventas.setId(this.registros.getInt("id"));
                ventas.setCodigo(this.registros.getString("codigo"));
                ventas.setTipo(Integer.parseInt(this.registros.getString("tipo")));
                ventas.setPrecio(Double.parseDouble(this.registros.getString("precio")));
                ventas.setCantidad(Integer.parseInt(this.registros.getString("cantidad")));
                ventas.setTotal(Double.parseDouble(this.registros.getString("total")));
                lista.add(ventas);
            }
        }
        this.Desconectar();
        return lista;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Ventas pro = new Ventas();
        if (this.Conectar()) {
            String consulta = "SELECT * from ventas where codigo = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //Asignar valores
            this.sqlConsulta.setString(1, codigo);
            //Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            if (this.registros.next()) {
                pro.setId(this.registros.getInt("id"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setTipo(Integer.parseInt(this.registros.getString("tipo")));
                pro.setPrecio(Double.parseDouble(this.registros.getString("precio")));
                pro.setCantidad(Integer.parseInt(this.registros.getString("cantidad")));
                pro.setTotal(Double.parseDouble(this.registros.getString("total")));
            } else {
                return -1;
            }
        }
        this.Desconectar();
        return pro;
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean res = false;
        Ventas ventas = new Ventas();
        if (this.Conectar()) {
            String consulta = "select * from ventas where codigo =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                res = true;
            }
        }
        this.Desconectar();
        return res;
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Ventas ventas = new Ventas();
        ventas = (Ventas) objecto;

        String consulta = "update ventas set codigo = ?, tipo = ?,"
                + " precio= ?, cantidad = ?, total = ? where codigo = ?";

        if (this.Conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //asignar valores a la consulta

                this.sqlConsulta.setString(1, ventas.getCodigo());
                this.sqlConsulta.setString(2, Integer.toString(ventas.getTipo()));
                if (ventas.getTipo() == 1) {
                    this.sqlConsulta.setString(3, Double.toString(24.50f));
                } else {
                    this.sqlConsulta.setString(3, Double.toString(20.50f));
                }
                this.sqlConsulta.setString(4, Integer.toString(ventas.getCantidad()));
                this.sqlConsulta.setString(5, Double.toString(ventas.getTotal()));
                this.sqlConsulta.setString(6, ventas.getCodigo());
                this.sqlConsulta.executeUpdate();

                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al Actualizar; " + e.getMessage());
            }
        }
    }
}

/*btnRegresar*/
