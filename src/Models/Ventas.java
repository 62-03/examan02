package Models;

public class Ventas {

    private int capacidad = 500;

    private int id;
    private String codigo;
    private Double precio;
    private int cantidad;
    private int tipo;
    private double total;

    public Ventas() {
    }

    public Ventas(int id, String codigo, Double precio, int cantidad, int tipo, double total) {
        this.id = id;
        this.codigo = codigo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.total = total;
    }

    public Ventas(Ventas ventas) {
        this.id = ventas.id;
        this.codigo = ventas.codigo;
        this.precio = ventas.precio;
        this.cantidad = ventas.cantidad;
        this.tipo = ventas.tipo;
        this.total = ventas.total;
    }

    public Double getPrecio() {
        return precio;
    }

    public int getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public double getTotal() {
        return total;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public double calcularTotal() {
        float costo = -1;
        if (cantidad <= capacidad) {
            if (getTipo() == 1) {
                //PREMIUM
                costo = (float) (cantidad * 24.50);
                capacidad -= cantidad;
            } else if (getTipo() == 2) {
                //REGULAR
                costo = (float) (cantidad * 20.50);
                capacidad -= cantidad;
            }
        }
        return costo;
    }
}

/*tblRegistros*/
