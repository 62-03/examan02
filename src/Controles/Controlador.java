package Controles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Views.*;
import Models.*;

public class Controlador implements ActionListener {

    dlgVenta vista1;
    dlgRegistros vista2;
    Ventas venta;
    dbVentas dbVenta;
    private boolean isActualizar = false;

    public Controlador(dlgVenta vista1, dlgRegistros vista2, Ventas venta, dbVentas dbVenta) {
        this.vista1 = vista1;
        this.vista2 = vista2;
        this.venta = venta;
        this.dbVenta = dbVenta;

        vista1.btnBuscar.addActionListener(this);
        vista1.btnNuevo.addActionListener(this);
        vista1.btnRealizar.addActionListener(this);
        vista1.btnGuardar.addActionListener(this);
        vista1.btnRegistros.addActionListener(this);
        vista2.btnMostrar.addActionListener(this);
        vista2.btnRegresar.addActionListener(this);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        dbVentas dbVenta = new dbVentas();
        dlgVenta vista1 = new dlgVenta(new JFrame(), true);
        dlgRegistros vista2 = new dlgRegistros(new JFrame(), true);

        Ventas venta = new Ventas();
        Controlador con = new Controlador(vista1, vista2, venta, dbVenta);

        con.iniVista1();

    }

    public void limpiar() {
        vista1.txtCodigo.setText("");
        vista1.txtCantidad.setText("");
        vista1.cmbTipos.setSelectedIndex(0);
        vista1.lblCantidad.setText("$ 0000.00");
        vista1.lblPrecio.setText("$ 0000.00");
        vista1.lblTotal.setText("$ 0000.00");
    }

    public void iniVista1() {
        vista1.setTitle(":: Registro de ventas de Gasolina ::");
        vista1.setSize(700, 700);
        vista1.setVisible(true);
    }

    public void iniVista2() {
        vista2.setTitle(":: Registro de ventas Realizadas ::");
        vista2.setSize(700, 700);
        vista2.setVisible(true);
    }

    private void tablaM() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Ventas> lista = new ArrayList<>();
        try {
            lista = dbVenta.listar();
        } catch (Exception ex) {
        }
        modelo.addColumn("codigo");
        modelo.addColumn("tipo");
        modelo.addColumn("precio");
        modelo.addColumn("cantidad");
        modelo.addColumn("total");
        for (Ventas ve : lista) {
            modelo.addRow(new Object[]{ve.getCodigo(), ve.getTipo(), ve.getPrecio(), ve.getCantidad(), ve.getTotal()});
        }
        vista2.tblRegistros.setModel(modelo);
    }

    //CONTROLADOR
    //MostrarTodo
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista1.btnNuevo) {
            vista1.txtCodigo.setEnabled(true);
            vista1.txtCantidad.setEnabled(true);
            vista1.cmbTipos.setEnabled(true);
            vista1.btnRealizar.setEnabled(true);
            vista1.btnGuardar.setEnabled(true);
            vista1.btnBuscar.setEnabled(true);
        }
        if (e.getSource() == vista1.btnRealizar) {
            int op = vista1.cmbTipos.getSelectedIndex();
            switch (op) {
                case 1:
                    venta.setTipo(1);
                    venta.setPrecio(24.50);
                    break;
                case 2:
                    venta.setTipo(2);
                    venta.setPrecio(20.50);
                    break;
                default:
                    JOptionPane.showMessageDialog(vista1, "Seleccione un Tipo de Gasolina");
                    return;
            }

            try {
                if (venta.calcularTotal() == -1.0) {
                    JOptionPane.showMessageDialog(vista1, "No hay gasolina suficiente");
                    return;
                }
                venta.setCodigo(vista1.txtCodigo.getText());
                venta.setCantidad(Integer.parseInt(vista1.txtCantidad.getText()));
                
                vista1.lblCantidad.setText(Integer.toString(venta.getCantidad()));
                vista1.lblPrecio.setText(Double.toString(venta.getPrecio()));
                vista1.lblTotal.setText(Double.toString(venta.calcularTotal()));
                venta.setTotal(Double.parseDouble(vista1.lblTotal.getText()));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista1, "No se pudo realizar la venta, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista1, "No se pudo realizar la venta, surgio el siguiente error: " + ex2.getMessage());
            }

        }
        if (e.getSource() == vista1.btnGuardar) {
            try {//isExiste
                if (!dbVenta.isExiste(venta.getCodigo())) {
                    dbVenta.insertar(venta);
                    limpiar();
                    JOptionPane.showMessageDialog(vista1, "Se Guardo con exito");
                    isActualizar=false;
                    return;
                }else if(isActualizar==true){
                    try {
                        dbVenta.actualizar(venta);  
                        JOptionPane.showMessageDialog(vista1, "Se actualizo con exito");
                        isActualizar=false;
                        return;
                    } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista1, "Fallo al actualizar, surgio el siguiente error: " + ex.getMessage());
                    } catch (Exception ex2) {
                        JOptionPane.showMessageDialog(vista1, "Fallo al actualizar, surgio el siguiente error: " + ex2.getMessage());
                    }   
                }
                limpiar();
                return;

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista1, "Fallo al guardar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista1, "Fallo al guardar, surgio el siguiente error: " + ex2.getMessage());
            }
        }
        if (e.getSource() == vista1.btnBuscar) {
            try {
                venta = (Ventas) dbVenta.buscar(vista1.txtCodigo.getText());
                venta.setCodigo(vista1.txtCodigo.getText());
                venta = venta;
                if (!dbVenta.buscar(venta.getCodigo()).equals(-1)) {
                    limpiar();
                    vista1.txtCodigo.setText(venta.getCodigo());
                    vista1.txtCantidad.setText(Integer.toString(venta.getCantidad()));
                    vista1.cmbTipos.setSelectedIndex(venta.getTipo());
                    vista1.lblCantidad.setText(Integer.toString(venta.getCantidad()));
                    vista1.lblPrecio.setText(Double.toString(venta.getPrecio()));
                    vista1.lblTotal.setText(Double.toString(venta.calcularTotal()));
                    this.isActualizar = true;
                } else {
                    JOptionPane.showMessageDialog(vista1, "No se encontro ");
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista1, "No se pudo encontrar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista1, "No se pudo encontrar, surgio el siguiente error: " + ex2.getMessage());
            }

        }
        if (e.getSource() == vista2.btnMostrar) {
            tablaM();
        }
        if (e.getSource() == vista1.btnRegistros) {
            vista1.setVisible(false);
            iniVista2();
        }
        if (e.getSource() == vista2.btnRegresar) {
            vista2.setVisible(false);
            iniVista1(); 
        }
    }
}

